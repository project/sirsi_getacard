/**
 * @file
 * JavaScript for sirsi_getacard module.
 */
(function ($, Drupal) {
  Drupal.behaviors.sirsi_getacard = {
    attach: function (context, settings) {
      if(typeof(context.forms) !== 'undefined') {
        const cardForm = document.getElementById('sirsi-getacard-form');
        const dataObj = JSON.parse(cardForm[0].value);
        let checkHome = cardForm[1];
        if (checkHome.id === 'edit-no-address') {
          checkHome.addEventListener('click', (e) => {
            if (e.target.checked === true) {
              cardForm.elements["street_add"].value = dataObj.admin_street_add;
              cardForm.elements["street_add_2"].value = dataObj.admin_street_add_2;
              cardForm.elements["city"].value = dataObj.admin_city;
              cardForm.elements["state"].value = dataObj.admin_state;
              cardForm.elements["zip"].value = dataObj.admin_zip;
              cardForm.elements["country"].value = dataObj.admin_country;
            }
          })
        }
      }
    }
  };
  Drupal.behaviors.cardFormReload = {
    attach: function (context, settings) {
      if(typeof(context.forms) !== 'undefined') {
        const localUrl = context.forms[0].action;
        const clearBtn = document.getElementById('edit-reset');
        clearBtn.addEventListener('click', function () {
          location.assign(localUrl);
        }, false);
      }
    }
  };
}(jQuery, Drupal));


