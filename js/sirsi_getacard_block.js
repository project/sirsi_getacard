/**
 * @file
 * JavaScript for sirsi_getacard module block and complete page.
 */
(function ($, Drupal) {
  Drupal.behaviors.accord = {
    attach: function (context, settings) {
      const formPanels = $('#accordion > dd');
      const formHeads = $('#accordion > dt');
      formPanels.hide();
      if (context.classList.contains('block-sirsi-getacard-block') || typeof(context.forms) === 'object') {
        formHeads.click(function () {
          formPanels.slideUp();
          $(this).next().slideDown();
          formHeads.removeClass('live-bar');
          $(this).addClass('live-bar');
          return false;
        })
      }
    }
  };
  Drupal.behaviors.accordComplete = {
    attach: function (context, settings) {
      if(context.title.includes('Registration Complete') && typeof(context.forms) === 'undefined') {
        const endPanels = $('#accordion > dd');
        const endHeads = $('#accordion > dt');
        endPanels.hide();
        endHeads.click(function () {
          endPanels.slideUp();
          $(this).next().slideDown();
          endHeads.removeClass('live-bar');
          $(this).addClass('live-bar');
          return false;
        })
      }
    }
  };
}(jQuery, Drupal));


