<?php
/**
 * @file
 * Contains \Drupal\sirsi_getacard\Service\SymphonyConnectorService.
 *
 */
namespace Drupal\sirsi_getacard\Service;

use Drupal;
use Drupal\Component\Serialization\Json;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * Class SymphonyConnectorService
 * @package Drupal\sirsi_getacard
 *
 */
class SymphonyConnectorService {

  /**
   * @var Client
   */
  private Client $httpClient;

  /**
   * @var string
   */
  private $apiUrl = '';

  /**
   * @var string
   */
  private $clientId = '';

  /**
   * @var string
   */
  private $originatingAppId = '';

  /**
   * @var string
   */
  private $apiUname = '';

  /**
   * @var string
   */
  private $apiPword = '';

  /**
   * @var
   */
  private $authToken;

  /**
   * SymphonyConnectorService constructor.
   */
  public function __construct() {
    $this->httpClient = Drupal::httpClient();
    $localConfig = Drupal::config('sirsi_getacard.adminsettings');
    $this->apiUrl = $localConfig->get('api_url');
    $this->clientId = $localConfig->get('client_id');
    $this->originatingAppId = $localConfig->get('webapp_id');
    $this->apiUname = $localConfig->get('apiuserid');
    $this->apiPword = $localConfig->get('apipasswd');
  }

  /**
   * @param $url
   * @param $headers
   * @param null $body
   * @return false|mixed
   * POST request to Symphony.
   */
  public function postToSymphony($url, $headers, $body = NULL) {
    $return = FALSE;
    $data = FALSE;
    $options['headers'] = $headers;
    $options['body'] = $body;
    $options['verify'] = FALSE;
    try {
      $response = $this->httpClient->post($url, $options);
      $status = $response->getStatusCode();
      if($status == '200') {
        $data = $response->getBody()->getContents();
      } else {
        Drupal::logger('sirsi_getacard')
          ->warning('postToSymphony() returned a status '.$status. ' with the response '.$response->getBody()
              ->getContents());
      }
    } catch (RequestException $e) {
      watchdog_exception('sirsi_getacard', $e);
    }
    if ($data) {
      $return = Json::decode($data);
    }
    return $return;
  }

  /**
   * @param $url
   * @param $params
   * @param $headers
   * @return false|mixed
   * GET request to Symphony.
   */
  public function getFromSymphony($url, $params, $headers) {
    $config = Drupal::service('config.factory')->getEditable('sirsi_getacard.adminsettings');
    $return = FALSE;
    $data = FALSE;
    try {
      $response = $this->httpClient->get($url, [
        'headers' => $headers,
        'query' => $params,
        'verify' => FALSE,
      ]);
      $status = $response->getStatusCode();
      if($status == '200') {
        $data = $response->getBody()->getContents();
      } else {
        Drupal::logger('sirsi_getacard')
          ->warning('getFromSymphony() returned a status '.$status. ' with the response '.$response->getBody()
              ->getContents());
      }
    } catch (RequestException $e) {
      watchdog_exception('sirsi_getacard ', $e);
      if($e->getCode() == '401') {
        $config->set('api_token', $this->getAuthToken())->save();
      }
    }
    if ($data) {
        $return = Json::decode($data);
    }
    return $return;
  }

  /**
   * @return false|mixed
   * gets a symphony api auth token
   */
  public function getAuthToken() {
    $return = FALSE;
    $body = [
      'login' => $this->apiUname,
      'password' => $this->apiPword
    ];
    $body = Json::encode($body);
    $headers = [
      'Accept' => 'application/json',
      'Content-Type' => 'application/json',
      'Accept-Encoding' => 'gzip, deflate, br',
      'Connection' => 'keep-alive',
      'SD-Originating-App-Id' => $this->originatingAppId,
      'x-sirs-clientID' => $this->clientId,
    ];
    $url = $this->apiUrl.'/user/staff/login';
    $data = $this->postToSymphony($url, $headers, $body);
    if ($data) {
      if(array_key_exists('sessionToken', $data)) {
        $return = $data['sessionToken'];
      }
    }
    $this->authToken = $return;
    return $return;
  }

  /**
   * @return array
   * makes headers for GET requests
   *
   */
  public function createGetHeaders() {
    $config = Drupal::service('config.factory')->getEditable('sirsi_getacard.adminsettings');
    if($this->authToken == NULL || $this->authToken == '') {
      $config->set('api_token', $this->getAuthToken())->save();
    }
    $headers = [
      'Accept' => 'application/json',
      'SD-Originating-App-Id' => $this->originatingAppId,
      'x-sirs-clientID' => $this->clientId,
      'x-sirs-sessionToken' => $this->authToken,
    ];
    return $headers;
  }

  /**
   * @param null|string $eMail
   * @return false|string
   * returns the patrons ID number or false if no match
   */
  public function emailLookup($eMail = NULL) {
    $return = FALSE;
    $headers = $this->createGetHeaders();
    $url = $this->apiUrl.'/user/patron/search';
    $params = [
      'q' => 'EMAIL:'.$eMail
    ];
    $data = $this->getFromSymphony($url, $params, $headers);
    if ($data) {
      if($data['totalResults'] == '1') {
        if(array_key_exists('key', $data['result'][0])) {
          $return = $data['result'][0]['key'];
        }
      }
    }
    return $return;
  }

  /**
   * @param null $name
   * @return false|string
   * returns the patrons barcode or false if no match
   */
  public function nameLookup($name = NULL) {
    $return = FALSE;
    $headers = $this->createGetHeaders();
    $params = [
      'q' => 'NAME:'.$name
    ];
    $url = $this->apiUrl.'/user/patron/search';
    $data = $this->getFromSymphony($url, $params, $headers);
    if ($data) {
      if($data['totalResults'] == '1') {
        if (array_key_exists('key', $data['result'][0])) {
          $return = $data['result'][0]['key'];
        }
      }
    }
    return $return;
  }

  /**
   * @param null|array $formValues
   * @return false|array
   * Creates a library patron
   *
   */
  public function createPatron($formValues = NULL) {
    $config = Drupal::service('config.factory')->getEditable('sirsi_getacard.adminsettings');
    $catKeyString = $config->get('language_field_key');
    $catKeypath = $config->get('language_field_path');
    $authHeaderString = $config->get('auth_header_str');
    $return = FALSE;
    if($formValues['street_add_2'] != NULL ||
      $formValues['street_add_2'] != '' ||
      $formValues['street_add_2'] !=' ') {
      $street_add = $formValues['street_add'].' '.$formValues['street_add_2'];
    }
    else {
      $street_add = $formValues['street_add'];
    }
    $homeLibrary = $formValues['home_library'];
    $chosenLanguage = $formValues['language_pref'];
    $authToken = $this->getAuthToken();
    $body = [
      'resource' => '/user/patron',
      'fields' => [
        'barcode' => 'AUTO',
        'firstName' => $formValues['first_name'],
        'middleName' => $formValues['mi'],
        'lastName' => $formValues['last_name'],
        'birthDate' => $formValues['dob'],
        'pin' => $formValues['pin'],
        'profile' => [
          'resource' => '/policy/userProfile',
          'key' => 'CUSTOMER',
        ],
        'library' => [
          'resource' => '/policy/library',
          'key' => $homeLibrary,
        ],
        "address1" => [
          [
            "resource" =>"/user/patron/address1",
            "fields" => [
              "code" => [
                "resource" => "/policy/patronAddress1",
                "key" => "STREET"
              ],
              "data" => $street_add
            ],
          ],
          [
            "resource" =>"/user/patron/address1",
            "fields" => [
              "code" => [
                "resource" => "/policy/patronAddress1",
                "key" => "CITY/STATE"
              ],
              "data" => $formValues['city'].', '.$formValues['state']
            ],
          ],
          [
            "resource" =>"/user/patron/address1",
            "fields" => [
              "code" => [
                "resource" => "/policy/patronAddress1",
                "key" => "ZIP"
              ],
              "data" => $formValues['zip']
            ],
          ],
          [
            "resource" =>"/user/patron/address1",
            "fields" => [
              "code" => [
                "resource" => "/policy/patronAddress1",
                "key" => "EMAIL"
              ],
              "data" => $formValues['email']
            ],
          ],
          [
            "resource" =>"/user/patron/address1",
            "fields" => [
              "code" => [
                "resource" => "/policy/patronAddress1",
                "key" => "PHONE"
              ],
              "data" => $formValues['phone_number']
            ],
          ],
          [
            "resource" =>"/user/patron/address1",
            "fields" => [
              "code" => [
                "resource" => "/policy/patronAddress1",
                "key" => "SCHOOL"
              ],
              "data" => $formValues['school_name']
            ],
          ],
        ],
      ]
    ];
    if(!empty($catKeyString) && !empty($catKeypath)) {
      $body['fields'][$catKeyString] = [
        'resource' => $catKeypath,
        'key' => $chosenLanguage,
      ];
    }
    $body = Json::encode($body);
    $headers = [
      'Accept' => 'application/json',
      'Content-Type' => 'application/json',
      'Accept-Encoding' => 'gzip, deflate, br',
      'Connection' => 'keep-alive',
      'SD-Originating-App-Id' => $this->originatingAppId,
      'x-sirs-clientID' => $this->clientId,
      'x-sirs-sessionToken' => $authToken,
      'SD-Working-LibraryID' => $homeLibrary,
      'SD-Prompt-Return' => $authHeaderString,
    ];
    $url = $this->apiUrl.'/user/patron';
    $data = $this->postToSymphony($url, $headers, $body);
    if ($data) {
      if(array_key_exists('key', $data)) {
        $return = $data['fields']['barcode'];
      }
    }
    return $return;
  }

  /**
   * @param null|string $idString
   * @return false|string
   * returns library patron info from SirsiDynix
   */
  public function returnUserInfo($idString = NULL) {
    $return = FALSE;
    $headers = $this->createGetHeaders();
    $url = $this->apiUrl.'/user/patron/key/'.$idString;
    $params = [
      'includeFields' => 'birthDate,address1,barcode,pin,firstName,lastName'
    ];
    $data = $this->getFromSymphony($url, $params, $headers);
    if ($data) {
        if (array_key_exists('fields', $data)) {
          $return = $data;
        }
    }
    return $return;
  }

  /**
   * @param null $id
   * @return false|mixed
   * returns a book record
   */
  public function getItemById($id = NULL) {
    $return = FALSE;
    $bibRecordArray = [];
    $headers = $this->createGetHeaders();
    $url = $this->apiUrl.'/catalog/bib/key/'.$id;
    $params = [];
    $data = $this->getFromSymphony($url, $params, $headers);
    if ($data) {
      /* build the bib record array */
      foreach($data['fields']['bib'] as $item) {
        if(is_array($item)) {
          $bibRecordArray[] = $item;
        }
      }
      $return[$data['key']] = [
        'author' => $data['fields']['author'],
        'title' => $data['fields']['title'],
        'bib' => $bibRecordArray,
      ];
    }
    return $return;
  }

}


