<?php
/**
 * @file
 * Contains \Drupal\sirsi_getacard\Service\SmartyConnectorService.
 */
namespace Drupal\sirsi_getacard\Service;

use Drupal;
use Drupal\Component\Serialization\Json;
use GuzzleHttp\Exception\RequestException;

/**
 * Class SmartyConnectorService
 * @package Drupal\sirsi_getacard
 *
 */
class SmartyConnectorService {

  /**
   * @var \GuzzleHttp\Client
   */
  private $httpClient = '';

  /**
   * @var array|mixed|null
   */
  private $baseUrl = '';

  /**
   * @var array|mixed|null
   */
  private $authId = '';

  /**
   * @var array|mixed|null
   */
  private $authToken = '';

  /**
   * SmartyConnectorService constructor.
   */
  public function __construct() {
    $this->httpClient = Drupal::httpClient();
    $localConfig = Drupal::config('sirsi_getacard.adminsettings');
    $this->baseUrl = $localConfig->get('base_url');
    $this->authId = $localConfig->get('auth_id');
    $this->authToken = $localConfig->get('auth_token');
  }

  /**
   * @param $url
   * @param $params
   * @param $headers
   * @return false|mixed
   * GET request to Smarty Streets.
   */
  protected function getFromSmarty($url, $params, $headers) {
    $return = FALSE;
    $data = FALSE;
    try {
      $response = $this->httpClient->get($url, [
        'headers' => $headers,
        'query' => $params,
        'verify' => FALSE,
      ]);
      $status = $response->getStatusCode();
      if($status == '200') {
        $data = $response->getBody()->getContents();
      } else {
        Drupal::logger('sirsi_getacard')
          ->warning('getFromSmarty() returned a status '.$status. ' with the response '.$response->getBody()
              ->getContents());
      }
    } catch (RequestException $e) {
      watchdog_exception('sirsi_getacard', $e);
    }
    if ($data) {
      $return = Json::decode($data);
    }
    return $return;
  }

  /**
   * @param $street_add
   * @param $city
   * @param $state
   * @param $zip
   * @return false|mixed
   *
   */
  public function checkAddress($street_add, $city, $state, $zip) {
    $url = $this->baseUrl;
    $headers = [
      'Content-Type' => 'application/json',
      'Host' => 'us-street.api.smartystreets.com',
    ];
    $params = [
      'auth-id' => $this->authId,
      'auth-token' => $this->authToken,
      'street' => $street_add,
      'city' => $city,
      'state' => $state,
      'zipcode' => $zip,
      'candidates' => '10',
    ];
    $data = $this->getFromSmarty($url, $params, $headers);
    ($data) ? $return = $data : $return = FALSE;
    return $return;
  }

}


