<?php

namespace Drupal\sirsi_getacard\Plugin\Block;

use Drupal;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;


/**
 * Provides a Get-A-Card Block.
 *
 * @Block(
 *   id = "sirsi_getacard_block",
 *   admin_label = @Translation("Sirsi Get-A-Card Block"),
 * )
 */
class SirsiGetacardBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $build = [
      '#theme' => 'sirsi_getacard_block',
      '#attached' => [
        'library' => [
          'sirsi_getacard/sirsi_getacard_block.library',
        ],
      ],
      '#getacard' => $this->buildSirsiGetacardBlock(),
      '#accord' => $this->buildSirsiGetacardAccordian($config),
    ];
    /* no cache */
    $build['#cache']['max-age'] = 0;
    return $build;
  }

  /**
   * @return int
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $markup = 'Get-A-Card Block Configuration.';
    $form['description'] = array(
      '#markup' => $markup,
    );

    $form['sirsi_getacard_block_header_one'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Heading One'),
      '#description' => $this->t('The heading of the first accordion section.'),
      '#default_value' => isset($config['sirsi_getacard_block_header_one']) ? $config['sirsi_getacard_block_header_one'] : '',
    );

    $form['sirsi_getacard_block_text_one'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Text Area One'),
      '#description' => $this->t('The content of the first accordion section.'),
      '#default_value' => isset($config['sirsi_getacard_block_text_one']) ? $config['sirsi_getacard_block_text_one'] : '',
    );

    $form['sirsi_getacard_block_header_two'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Heading Two'),
      '#description' => $this->t('The heading of the second accordion section.'),
      '#default_value' => isset($config['sirsi_getacard_block_header_two']) ? $config['sirsi_getacard_block_header_two'] : '',
    );

    $form['sirsi_getacard_block_text_two'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Text Area Two'),
      '#description' => $this->t('The content of the second accordion section.'),
      '#default_value' => isset($config['sirsi_getacard_block_text_two']) ? $config['sirsi_getacard_block_text_two'] : '',
    );

    $form['sirsi_getacard_block_header_three'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Heading Three'),
      '#description' => $this->t('The heading of the third accordion section.'),
      '#default_value' => isset($config['sirsi_getacard_block_header_three']) ? $config['sirsi_getacard_block_header_three'] : '',
    );

    $form['sirsi_getacard_block_text_three'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Text Area Three'),
      '#description' => $this->t('The content of the third accordion section.'),
      '#default_value' => isset($config['sirsi_getacard_block_text_three']) ? $config['sirsi_getacard_block_text_three'] : '',
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['sirsi_getacard_block_header_one'] = $form_state->getValue('sirsi_getacard_block_header_one');
    $this->configuration['sirsi_getacard_block_text_one'] = $form_state->getValue('sirsi_getacard_block_text_one');
    $this->configuration['sirsi_getacard_block_header_two'] = $form_state->getValue('sirsi_getacard_block_header_two');
    $this->configuration['sirsi_getacard_block_text_two'] = $form_state->getValue('sirsi_getacard_block_text_two');
    $this->configuration['sirsi_getacard_block_header_three'] = $form_state->getValue('sirsi_getacard_block_header_three');
    $this->configuration['sirsi_getacard_block_text_three'] = $form_state->getValue('sirsi_getacard_block_text_three');
  }

  /**
   * @param  array $config
   * @return array
   */
  public function buildSirsiGetacardBlock() {
    $getacardForm = Drupal::formBuilder()->getForm('Drupal\sirsi_getacard\Form\getacardForm');
    $rendered[] = [$getacardForm];
    return $rendered;
  }

  public function buildSirsiGetacardAccordian($config) {

    $render = [];
    if ($config['sirsi_getacard_block_header_one'] != NULL || $config['sirsi_getacard_block_header_one'] != '') {
      $render['header_one'] = $config['sirsi_getacard_block_header_one'];
    }
    if ($config['sirsi_getacard_block_text_one'] != NULL || $config['sirsi_getacard_block_text_one'] != '') {
      $render['text_one'] = $config['sirsi_getacard_block_text_one'];
    }
    if ($config['sirsi_getacard_block_header_two'] != NULL || $config['sirsi_getacard_block_header_two'] != '') {
      $render['header_two'] = $config['sirsi_getacard_block_header_two'];
    }
    if ($config['sirsi_getacard_block_text_two'] != NULL || $config['sirsi_getacard_block_text_two'] != '') {
      $render['text_two'] = $config['sirsi_getacard_block_text_two'];
    }
    if ($config['sirsi_getacard_block_header_three'] != NULL || $config['sirsi_getacard_block_header_three'] != '') {
      $render['header_three'] = $config['sirsi_getacard_block_header_three'];
    }
    if ($config['sirsi_getacard_block_text_three'] != NULL || $config['sirsi_getacard_block_text_three'] != '') {
      $render['text_three'] = $config['sirsi_getacard_block_text_three'];
    }
    $textString = '<div id="accordion">';
    foreach($render as $id => $element) {
      switch ($id) {
        case 'header_one':
          $textString .= '<dt>'.$element.'</dt>';
          break;
        case 'text_one':
          $textString .= '<dd>'.$element.'</dd>';
          break;
        case 'header_two':
          $textString .= '<dt>'.$element.'</dt>';
          break;
        case 'text_two':
          $textString .= '<dd>'.$element.'</dd>';
          break;
        case 'header_three':
          $textString .= '<dt>'.$element.'</dt>';
          break;
        case 'text_three':
          $textString .= '<dd>'.$element.'</dd>';
          break;
      }
    }
    $textString .= '</div>';
    $rendered[] = [
      '#markup' => $textString,
    ];
    return $rendered;
  }


}
