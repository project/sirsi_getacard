<?php
/**
 * @file
 * Contains \Drupal\sirsi_getacard\Controller\pageController.
 */
namespace Drupal\sirsi_getacard\Controller;

use Drupal;
use Com\Tecnick\Barcode\Barcode as BarcodeGenerator;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StreamWrapper\PublicStream;

class pageController extends ControllerBase {


  /**
   * @param null $barcode
   * @return string[]
   * @throws \Com\Tecnick\Barcode\Exception
   *
   */
  public function complete($barcode=NULL) {
    $thankYouText = Drupal::config('sirsi_getacard.adminsettings')->get('thankyou_message_one');
    $renderer = Drupal::service('renderer');
    $barcode_render_array = [];
    $path = Drupal::service('file_system')->realpath('.') . '/' . PublicStream::basePath().'/barcodes';
    if (!Drupal::service('file_system')->prepareDirectory($path)) {
      Drupal::service('file_system')->mkdir($path);
    }
    $file_path_physical = $path . '/' . $barcode .'.png';
    $generator = new BarcodeGenerator();
    $code = $generator->getBarcodeObj('C39', $barcode, -2, -40, 'black', array(0, 0, 0, 0));
    $imageData = $code->getPngData();
    $savedFile = Drupal::service('file_system')->saveData($imageData, $file_path_physical, FileSystemInterface::EXISTS_REPLACE);
    $image = Drupal::service('image.factory')->get($savedFile);
    if ($image->isValid()) {
      $barcode_render_array = [
        '#theme' => 'image_style',
        '#width' => $image->getWidth(),
        '#height' => $image->getHeight(),
        '#style_name' => 'large',
        '#uri' => 'public://barcodes/' . $barcode . '.png',
      ];
    }
    $var = '';
    $var .= '<div class="confirmation-container">';
    $var .= '<div class="two_thirds">';
    $var .= '<strong><p>Thank you for registering. Please bookmark this page.</p></strong><br>';
    $var .= '<p>'.$thankYouText.'</p>';
    $var .= '<div>';
    $var .= $barcode;
    $var .= '</div>';
    if($barcode_render_array) {
      $var .= '<div>';
      $var .= $renderer->render($barcode_render_array);
      $var .= '</div>';
    }
    $var .= '</div>';
    $var .= '<div class="one_third">';
    $var .= $this->buildAccordian();
    $var .= '</div>';
    $var .= '</div>';
    $element = [
      '#type' => 'markup',
      '#title' => 'Registration Complete',
      '#markup' => $var,
      '#attached' => [
        'library' => [
          'sirsi_getacard/sirsi_getacard_block.library',
        ],
      ],
    ];
    return $element;
  }

  /**
   * @return string
   *
   */
  public function buildAccordian() {
    $config = Drupal::config('sirsi_getacard.adminsettings');
    $render = [];
    $header1 = $config->get('header_one');
    if ($header1 != NULL && $header1 != '') {
      $render['header_one'] = $header1;
    }
    $text1 = $config->get('text_one');
    if ($text1 != NULL && $text1 != '') {
      $render['text_one'] = $text1;
    }
    $header2 = $config->get('header_two');
    if ($header2 != NULL && $header2 != '') {
      $render['header_two'] = $header2;
    }
    $text2 = $config->get('text_two');
    if ($text2 != NULL && $text2 != '') {
      $render['text_two'] = $text2;
    }
    $header3 = $config->get('header_three');
    if ($header3 != NULL && $header3 != '') {
      $render['header_three'] = $header3;
    }
    $text3 = $config->get('text_three');
    if ($text3 != NULL && $text3 != '') {
      $render['text_three'] = $text3;
    }
    $header4 = $config->get('header_four');
    if ($header4 != NULL && $header4 != '') {
      $render['header_four'] = $header4;
    }
    $text4 = $config->get('text_four');
    if ($text4 != NULL && $text4 != '') {
      $render['text_four'] = $text4;
    }
    $header5 = $config->get('header_five');
    if ($header5 != NULL && $header5 != '') {
      $render['header_five'] = $header5;
    }
    $text5 = $config->get('text_five');
    if ($text5 != NULL && $text5 != '') {
      $render['text_five'] = $text5;
    }
    $header6 = $config->get('header_six');
    if ($header6 != NULL && $header6 != '') {
      $render['header_six'] = $header6;
    }
    $text6 = $config->get('text_six');
    if ($text6 != NULL && $text6 != '') {
      $render['text_six'] = $text6;
    }
    $textString = '<div id="accordion">';
    foreach($render as $id => $element) {
      switch ($id) {
        case 'header_one':
          $textString .= '<dt>'.$element.'</dt>';
          break;
        case 'text_one':
          $textString .= '<dd>'.$element.'</dd>';
          break;
        case 'header_two':
          $textString .= '<dt>'.$element.'</dt>';
          break;
        case 'text_two':
          $textString .= '<dd>'.$element.'</dd>';
          break;
        case 'header_three':
          $textString .= '<dt>'.$element.'</dt>';
          break;
        case 'text_three':
          $textString .= '<dd>'.$element.'</dd>';
          break;
        case 'header_four':
          $textString .= '<dt>'.$element.'</dt>';
          break;
        case 'text_four':
          $textString .= '<dd>'.$element.'</dd>';
          break;
        case 'header_five':
          $textString .= '<dt>'.$element.'</dt>';
          break;
        case 'text_five':
          $textString .= '<dd>'.$element.'</dd>';
          break;
        case 'header_six':
          $textString .= '<dt>'.$element.'</dt>';
          break;
        case 'text_six':
          $textString .= '<dd>'.$element.'</dd>';
          break;
      }
    }
    $textString .= '</div>';
    return $textString;
  }

}
