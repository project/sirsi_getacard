<?php

namespace Drupal\sirsi_getacard\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Component\Utility\EmailValidator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal;

/**
 * Provides a form with multiple steps.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class getacardForm extends FormBase {

  /**
   * The mail manager.
   *
   * @var Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The email validator.
   *
   * @var Drupal\Component\Utility\EmailValidator
   */
  protected $emailValidator;

  /**
   * Drupal config factory interface.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The language manager.
   *
   * @var Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * getacardForm constructor.
   * @param MailManagerInterface $mail_manager
   * @param LanguageManagerInterface $language_manager
   * @param EmailValidator $email_validator
   * @param ConfigFactoryInterface $config_factory
   */
  public function __construct(
    MailManagerInterface $mail_manager,
    LanguageManagerInterface $language_manager,
    EmailValidator $email_validator,
    ConfigFactoryInterface $config_factory
  ) {
    $this->mailManager = $mail_manager;
    $this->languageManager = $language_manager;
    $this->emailValidator = $email_validator;
    $this->configFactory = $config_factory;
  }

  /**
   * @param ContainerInterface $container
   * @return getacardForm|static
   *
   */
  public static function create(ContainerInterface $container) {
    $form = new static(
      $container->get('plugin.manager.mail'),
      $container->get('language_manager'),
      $container->get('email.validator'),
      $container->get('config.factory')
    );
    $form->setMessenger($container->get('messenger'));
    $form->setStringTranslation($container->get('string_translation'));
    return $form;
  }

  /**
   * @return string
   *
   */
  public function getFormId() {
    return 'sirsi_getacard_form';
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   *
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $testMode = $this->configFactory->get('sirsi_getacard.adminsettings')->get('test_mode');
    $renderer = \Drupal::service('renderer');
    if($testMode != 0) {
      $this->messenger()->addWarning('Signup Forms are in Test Mode');
      $required = FALSE;
    } else {
      $required = TRUE;
    }
    if ($form_state->has('page_num') && $form_state->get('page_num') == 2) {
      return self::createPageTwo($form, $form_state);
    }
    if ($form_state->has('page_num') && $form_state->get('page_num') == 3) {
      return self::createPageThree($form, $form_state);
    }
    $form_state->set('page_num', 1);
    /* voter registration support */
    $regEnabled = $this->configFactory->get('sirsi_getacard.adminsettings')->get('enable_reg');
    if($regEnabled != 0) {
      $voterRegPath = $this->configFactory->get('sirsi_getacard.adminsettings')->get('voterreg');
      $fullVoterRegPath = Drupal::request()->getRequestUri() . $voterRegPath;
      $cardUrl = Url::fromUserInput($fullVoterRegPath);
      /* avoid complications caused by missing files in the file system */
      $imageArray = $this->configFactory->get('sirsi_getacard.adminsettings')->get('voterreg_img');
      (isset($imageArray[0])) ? $renderable = sirsi_getacard_FormImage($imageArray[0]) : $renderable = FALSE;
      if($renderable != FALSE) {
        $markup = '<a href="'.$cardUrl->toString().'">'.$renderer->render($renderable).'</a>';
      }
      else {
        $voterRegLink = new Link('Register To Vote', $cardUrl);
        $markup = $voterRegLink->toString()->getGeneratedLink();
      }
      $form['register'] = array(
        '#prefix' => '<div id="voter-reg">',
        '#suffix' => '</div>',
        '#markup' => $markup,
      );
    }

    $form['sub_title'] = array(
      '#markup' => $this->t('Library card application and information about card benefits.'),
    );
    /* markup for tabs */
    $tabMarkup = '<div id="formTabs" class="tab tabsPage1">';
    $tabMarkup .= '<div id="formTab1" class="formbutton cell medium-auto active">1. Personal Details</div>';
    $tabMarkup .= '<div id="formTab2" class="formbutton cell medium-auto unclicked" >2. Address Verification</div>';
    $tabMarkup .= '<div id="formTab3" class="formbutton cell medium-auto unclicked">3. Confirmation</div>';
    $tabMarkup .= '</div>';
    $form['tabs'] = array(
      '#markup' => $tabMarkup,
    );

    $form['sirsi-hidden'] = array(
      '#type' => 'hidden',
      '#value' => sirsi_getacardMakeUnhousedValues(),
    );

    $form['personal'] = array(
      '#type' => 'fieldset',
      '#open' => TRUE,
    );

    $form['personal']['first_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('First Name'),
      '#default_value' => $form_state->getValue('first_name'),
      '#required' => $required,
    ];

    $form['personal']['mi'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Middle Name'),
      '#default_value' => $form_state->getValue('mi'),
    ];

    $form['personal']['last_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Last Name'),
      '#default_value' => $form_state->getValue('last_name'),
      '#required' => $required,
    ];

    $form['personal']['preferred'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Preferred / Chosen Name'),
      '#default_value' => $form_state->getValue('preferred'),
      '#required' => FALSE,
    ];

    $dateFormat = "Y-m-d";
    $savedDate = $form_state->getValue('dob');
    $eDate = date($dateFormat);
    $form['personal']['dob'] = [
      '#type' => 'date',
      '#title' => $this->t('Date of Birth'),
      '#default_value' => ($savedDate) ? $form_state->getValue('dob') : $eDate,
      '#required' => $required,
    ];

    $form['personal']['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email Address'),
      '#default_value' => $form_state->getValue('email'),
      '#required' => $required,
    ];

    $form['personal']['confirm_email'] = [
      '#type' => 'email',
      '#title' => $this->t('Confirm Email'),
      '#default_value' => $form_state->getValue('confirm_email'),
      '#required' => $required,
    ];

    $form['personal']['phone_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Phone Number'),
      '#default_value' => $form_state->getValue('phone_number'),
      '#required' => $required,
    ];

    $form['personal']['language_pref'] = [
      '#type' => 'select',
      '#title' => $this->t('Primary Language Spoken'),
      '#required' => FALSE,
      '#options' => sirsi_getacardLangDropdown(),
      '#empty_option' => $this->t('-select-'),
      '#default_value' => $form_state->getValue('language_pref'),
    ];


    if ($form_state->has('underageFlag') && $form_state->get('underageFlag') == 1) {

      $form['parental'] = array(
        '#type' => 'fieldset',
        '#title' => $this->t('Parental Personal Information'),
        '#open' => TRUE,
      );

      $form['parental']['p_first_name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Parent First Name'),
        '#default_value' => $form_state->getValue('p_first_name'),
        '#required' => $required,
      ];

      $form['parental']['p_mi'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Parent Middle Initial'),
        '#default_value' => $form_state->getValue('p_mi'),
      ];

      $form['parental']['p_last_name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Parent Last Name'),
        '#default_value' => $form_state->getValue('p_last_name'),
        '#required' => $required,
      ];

      $form['parental']['p_email'] = [
        '#type' => 'email',
        '#title' => $this->t('Parent Email Address'),
        '#default_value' => $form_state->getValue('p_email'),
        '#required' => $required,
      ];

      $form['parental']['p_phone_number'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Parent Phone Number'),
        '#default_value' => $form_state->getValue('p_phone_number'),
        '#required' => $required,
      ];
    }

    $form['signup_terms'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Terms, Conditions and Privacy Policy'),
      '#open' => TRUE,
    );

    $termsPath = $this->configFactory->get('sirsi_getacard.adminsettings')->get('termsofuse');
    $fullTermsPath = Drupal::request()->getRequestUri() . $termsPath;
    $termsUrl = Url::fromUserInput($fullTermsPath);
    $termsLink = new Link('Terms and Conditions', $termsUrl);
    $form['signup_terms']['signature'] = [
      '#type' => 'checkbox',
      '#title' => 'I agree to the '.$termsLink->toString()->getGeneratedLink(),
      '#default_value' => $form_state->getValue('signature'),
      '#required' => $required,
    ];

    $privacyPath = $this->configFactory->get('sirsi_getacard.adminsettings')->get('privacy');
    $fullPrivacyPath = Drupal::request()->getRequestUri() . $privacyPath;
    $privacyUrl = Url::fromUserInput($fullPrivacyPath);
    $privacyLink = new Link('Privacy Policy', $privacyUrl);
    $form['signup_terms']['accept_priv'] = [
      '#type' => 'checkbox',
      '#title' => 'I accept the '.$privacyLink->toString()->getGeneratedLink(),
      '#default_value' => $form_state->getValue('accept_priv'),
      '#required' => $required,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['next'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Save and Continue'),
      '#prefix' => '<div id="sirsi-clr-btn">',
      '#suffix' => '</div>',
      '#submit' => ['::createNextSubmit'],
      '#validate' => ['::createNextValidate'],
    ];

    $form['actions']['reset'] = [
      '#type' => 'button',
      '#value' => $this->t('Clear'),
      '#prefix' => '<div id="sirsi-reset-btn">',
      '#suffix' => '</div>',
      '#attributes' => ['onclick' => 'return false;'],
    ];

    return $form;
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * Custom validation handler for page 1.
   */
  public function createNextValidate(array $form, FormStateInterface $form_state) {
    $verifyMail = FALSE;
    $verifyName = FALSE;
    $registeredEmail = 'Unregistered';
    $symphonyApiController = Drupal::service('sirsi_getacard_symphony.connector');
    $testMode = $this->configFactory->get('sirsi_getacard.adminsettings')->get('test_mode');

    $email = $form_state->getValue('email');
    $confirm_email = $form_state->getValue('confirm_email');

    if ($email != $confirm_email) {
      $this->messenger()->addWarning($this->t('Email addresses do not match.'));
      $form_state->setRebuild(TRUE);
    }
    if(!$this->emailValidator->isValid($email)) {
      $this->messenger()->addWarning($this->t('Email address is not valid.'));
      $form_state->setRebuild(TRUE);
    }
    $firstName = $form_state->getValue('first_name');
    $middleIni = $form_state->getValue('mi');
    $lastName = $form_state->getValue('last_name');

    if($middleIni != NULL && $middleIni != '') {
      $fullName = $firstName.' '.$middleIni.' '.$lastName;
    }
    else {
      $fullName = $firstName.' '.$lastName;
    }
    /* make sure the Symphony service is up and running */
    if($symphonyApiController->getAuthToken() == FALSE) {
      $this->messenger()->addError($this->t('Registration is currently offline. We apologize for any inconvenience'));
      $form_state->setRebuild(TRUE);
    }
    else {
      /* gtg */
      if($email != NULL && $email != '') {
        $verifyMail = $symphonyApiController->emailLookup($email);
      }
      if($fullName != NULL && $fullName != '' && $fullName != ' ') {
        $verifyName = $symphonyApiController->nameLookup($fullName);
      }
      if ($verifyMail != FALSE && $verifyName != FALSE) {
        $userInfo = $symphonyApiController->returnUserInfo($verifyMail);
        $statusMessage = 'Hi ' . $userInfo['fields']['firstName'] . '! You\'re already registered.';
        $statusMessage .= ' The email used to register this account is '.$email;
        $statusMessage .= ' We emailed your library card information to this address.';

        $mailMessage = 'Hi ' . $userInfo['fields']['firstName'];
        $mailMessage .= ' Your library card number is '.$userInfo['fields']['barcode'];

        $to = $email;
        $from = $this->config('system.site')->get('mail');
        $params = ['message' => $mailMessage];
        $language_code = $this->languageManager->getDefaultLanguage()->getId();

        $result = $this->mailManager->mail('sirsi_getacard', 'recover_account', $to, $language_code, $params, $from, TRUE);
        if ($result['result'] == TRUE) {
          $this->messenger()->addMessage($this->t($statusMessage));
        }
        else {
          Drupal::logger('sirsi_getacard')
            ->error('There was a problem sending an emailed message to '.$registeredEmail);
        }
        $form_state->setRebuild(TRUE);
      }
      elseif ($verifyMail != FALSE) {
        $userInfo = $symphonyApiController->returnUserInfo($verifyMail);
        if(array_key_exists('address1', $userInfo['fields'])) {
          $addressArray = $userInfo['fields']['address1'];
          foreach($addressArray as $value) {
            if($value['fields']['code']['key'] == 'EMAIL'){
              $registeredEmail = $value['fields']['data'];
            }
          }
        }
        $statusMessage = 'It looks like this email is already in use. ';
        $statusMessage .= ' The email address used to create this account is '.$registeredEmail;
        $statusMessage .= ' We emailed your library card information to this address.';

        $mailMessage = 'Hi ' . $userInfo['fields']['firstName'];
        $mailMessage .= ' Your library card number is '.$userInfo['fields']['barcode'];

        $to = $registeredEmail;
        $from = $this->config('system.site')->get('mail');
        $params = ['message' => $mailMessage];
        $language_code = $this->languageManager->getDefaultLanguage()->getId();

        $result = $this->mailManager->mail('sirsi_getacard', 'recover_account', $to, $language_code, $params, $from, TRUE);
        if ($result['result'] == TRUE) {
          $this->messenger()->addMessage($this->t($statusMessage));
        }
        else {
          Drupal::logger('sirsi_getacard')
            ->error('There was a problem sending an emailed message to '.$registeredEmail);
        }
        $form_state->setRebuild(TRUE);
      }
    }
    $birthDate = $form_state->getValue('dob');
    $userYearString = substr($birthDate, 0, 4);
    $dateFormat = "Y-m-d";
    $currentDate = date($dateFormat);
    $yearString = substr($currentDate, 0, 4);
    $minAge = $this->configFactory->get('sirsi_getacard.adminsettings')->get('min_age');
    $underAge = intval($yearString) - intval($minAge);
    /* check the date entered */
    if($birthDate == $currentDate) {
      if($testMode == 0) {
        $statusMessage = 'Please enter your birth date.';
        $this->messenger()->addStatus($this->t($statusMessage));
        $form_state->setRebuild(TRUE);
      }
    }
    elseif(intval($userYearString) >= $underAge) {
      $statusMessage = 'You need your parents information to sign up.';
      $this->messenger()->addStatus($this->t($statusMessage));
      $form_state->set('underageFlag', 1);
      $form_state->setRebuild(TRUE);
    }
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * Custom submission handler for page 1.
   */
  public function createNextSubmit(array &$form, FormStateInterface $form_state) {

    $form_vals = [
      'first_name' => $form_state->getValue('first_name'),
      'mi' => $form_state->getValue('mi'),
      'last_name' => $form_state->getValue('last_name'),
      'preferred' => $form_state->getValue('preferred'),
      'email' => $form_state->getValue('email'),
      'confirm_email' => $form_state->getValue('confirm_email'),
      'dob' => $form_state->getValue('dob'),
      'phone_number' => $form_state->getValue('phone_number'),
      'language_pref' => $form_state->getValue('language_pref'),
      'signature' => $form_state->getValue('signature'),
      'accept_priv' => $form_state->getValue('accept_priv'),
      'p_first_name' => $form_state->getValue('p_first_name'),
      'p_mi' => $form_state->getValue('p_mi'),
      'p_last_name' => $form_state->getValue('p_last_name'),
      'p_email' => $form_state->getValue('p_email'),
      'p_phone_number' => $form_state->getValue('p_phone_number'),
    ];

    $form_state
      ->set('page1_values', $form_vals)
      ->set('page_num', 2)
      ->setRebuild(TRUE);
  }

  ########################## END Form 1 ##########################

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   * Builds the second step form (page 2).
   */
  public function createPageTwo(array &$form, FormStateInterface $form_state) {
    $testMode = $this->configFactory->get('sirsi_getacard.adminsettings')->get('test_mode');
    if($testMode != 0) {
      $this->messenger()->addWarning('Signup Forms are in Test Mode');
      $required = FALSE;
    } else {
      $required = TRUE;
    }
    /* create a unique file location name */
    $pageOneVals = $form_state->get('page1_values');
    $uname = $pageOneVals['first_name'].'-'.$pageOneVals['last_name'].'-'.$pageOneVals['phone_number'];

    $form['sub_title'] = array(
      '#markup' => $this->t('Library card application and information about card benefits.'),
    );
    /* markup for tabs */
    $tabMarkup = '<div id="formTabs" class="tab tabsPage2">';
    $tabMarkup .= '<div id="formTab1" class="formbutton cell medium-auto completed">1. Personal Details</div>';
    $tabMarkup .= '<div id="formTab2" class="formbutton cell medium-auto active">2. Address Verification</div>';
    $tabMarkup .= '<div id="formTab3" class="formbutton cell medium-auto unclicked">3. Confirmation</div>';
    $tabMarkup .= '</div>';
    $form['tabs'] = array(
      '#markup' => $tabMarkup,
    );

    $form['sirsi-hidden'] = array(
      '#type' => 'hidden',
      '#value' => sirsi_getacardMakeUnhousedValues(),
    );

    $form['no_address'] = [
      '#type' => 'checkbox',
      '#title' => 'I do not have an address',
      '#default_value' => $form_state->getValue('no_address'),
    ];

    $form['educator'] = [
      '#type' => 'checkbox',
      '#title' => 'I\'m a local educator',
      '#default_value' => $form_state->getValue('educator'),
    ];

    $form['home_add'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Home Address'),
      '#open' => TRUE,
    );

    $form['home_add']['street_add'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Street Address'),
      '#required' => $required,
      '#default_value' => $form_state->getValue('street_add'),
      '#size' => 60,
      '#maxlength' => 128,
    ];

    $form['home_add']['street_add_2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Street Address 2'),
      '#default_value' => $form_state->getValue('street_add_2'),
      '#size' => 60,
      '#maxlength' => 128,
    ];

    $form['home_add']['city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City'),
      '#required' => $required,
      '#default_value' => $form_state->getValue('city'),
      '#size' => 60,
      '#maxlength' => 128,
    ];

    $form['home_add']['state'] = [
      '#type' => 'select',
      '#title' => $this->t('State'),
      '#required' => $required,
      '#options' => sirsi_getacard_StatesDropdown(),
      '#empty_option' => $this->t('-select-'),
      '#default_value' => $form_state->getValue('state'),
    ];

    $form['home_add']['country'] = [
      '#type' => 'select',
      '#title' => $this->t('Country'),
      '#required' => $required,
      '#options' => [
        'US' => $this->t('United States'),
      ],
      '#empty_option' => $this->t('-select-'),
      '#default_value' => $form_state->getValue('country'),
    ];

    $form['home_add']['zip'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Zip Code'),
      '#required' => $required,
      '#default_value' => $form_state->getValue('zip'),
      '#size' => 60,
      '#maxlength' => 128,
    ];

    $form['edu_fields'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Educator Information'),
      '#open' => TRUE,
      '#states' => [
        'invisible' => [
          ':input[name="educator"]' => ['checked' => FALSE],
        ],
      ],
    );

    $form['edu_fields']['info'] = [
      '#markup' => 'Please upload a copy of your Work ID, Paystub, Supervisor Letter, or Intent/Continue to Homeschool Document',
      ];

    $form['edu_fields']['id_one'] = [
      '#type' => 'managed_file',
      '#title' => 'Front',
      '#description' => 'You can upload .jpg, .gif, .png, .txt, .doc, .xls',
      '#upload_location' => 'public://signups/'.$uname.'/',
      '#upload_validators' => [
        'file_validate_is_image' => array(),
        'file_validate_extensions' => array('jpg jpeg gif png txt doc xls pdf ppt pps odt ods odp'),
      ],
      '#default_value' => $form_state->getValue('id_one'),
    ];

    $form['edu_fields']['id_one_back'] = [
      '#type' => 'managed_file',
      '#title' => 'Back',
      '#description' => 'You can upload .jpg, .gif, .png, .txt, .doc, .xls',
      '#upload_location' => 'public://signups/'.$uname.'/',
      '#upload_validators' => [
        'file_validate_is_image' => array(),
        'file_validate_extensions' => array('jpg jpeg gif png txt doc xls pdf ppt pps odt ods odp'),
      ],
      '#default_value' => $form_state->getValue('id_one_back'),
    ];

    $form['edu_fields']['school_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('School Name'),
      '#default_value' => $form_state->getValue('school_name'),
      '#size' => 60,
      '#maxlength' => 128,
    ];

    $form['edu_fields']['edu_street_add'] = [
      '#type' => 'textfield',
      '#title' => $this->t('School Street Address'),
      '#default_value' => $form_state->getValue('edu_street_add'),
      '#size' => 60,
      '#maxlength' => 128,
    ];

    $form['edu_fields']['edu_street_add_2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('School Street Address 2'),
      '#default_value' => $form_state->getValue('edu_street_add_2'),
      '#size' => 60,
      '#maxlength' => 128,
    ];

    $form['edu_fields']['edu_city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('School City'),
      '#default_value' => $form_state->getValue('edu_city'),
      '#size' => 60,
      '#maxlength' => 128,
    ];

    $form['edu_fields']['edu_state'] = [
      '#type' => 'select',
      '#title' => $this->t('School State'),
      '#options' => sirsi_getacard_StatesDropdown(),
      '#empty_option' => $this->t('-select-'),
      '#default_value' => 'DC',
    ];

    $form['additional'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Additional Info'),
      '#open' => TRUE,
    );

    $form['additional']['home_library'] = [
      '#type' => 'select',
      '#title' => $this->t('Home Library'),
      '#options' => sirsi_getacardLibDropdown(),
      '#empty_option' => $this->t('-select-'),
      '#default_value' => $form_state->getValue('home_library'),
    ];

    $form['additional']['pin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Create PIN'),
      '#required' => $required,
      '#default_value' => $form_state->getValue('pin'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['back'] = [
      '#type' => 'submit',
      '#value' => $this->t('Previous'),
      '#prefix' => '<div id="sirsi-clr-btn">',
      '#suffix' => '</div>',
      '#submit' => ['::createPageTwoBack'],
      '#limit_validation_errors' => [],
    ];

    $form['actions']['next2'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Next'),
      '#prefix' => '<div id="sirsi-clr-btn">',
      '#suffix' => '</div>',
      '#submit' => ['::createPage2Submit'],
      '#validate' => ['::createPage2Validate'],
    ];

    $form['actions']['reset'] = [
      '#type' => 'button',
      '#value' => $this->t('Clear'),
      '#prefix' => '<div id="sirsi-reset-btn">',
      '#suffix' => '</div>',
      '#attributes' => ['onclick' => 'return false;'],
    ];

    return $form;
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * Submission handler for 'Back' button (page 2).
   */
  public function createPageTwoBack(array &$form, FormStateInterface $form_state) {
    $form_state
      ->setValues($form_state->get('page1_values'))
      ->set('page_num', 1)
      ->setRebuild(TRUE);
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * Validation handler for page 2.
   *
   */
  public function createPage2Validate(array &$form, FormStateInterface $form_state) {
    $streetsController = Drupal::service('sirsi_getacard_smarty.connector');
    if($form_state->getValue('street_add_2') != NULL ||
      $form_state->getValue('street_add_2') != '' ||
      $form_state->getValue('street_add_2') !=' ') {
      $street_add = $form_state->getValue('street_add').' '.$form_state->getValue('street_add_2');
    }
    else {
      $street_add = $form_state->getValue('street_add');
    }
    $city = $form_state->getValue('city');
    $state = $form_state->getValue('state');
    /* $country = $form_state->getValue('country'); */
    $zip = $form_state->getValue('zip');

    $response = $streetsController->checkAddress($street_add, $city, $state, $zip);
    if($response) {
      if (array_key_exists('components', $response[0])) {
        $componentArray = $response[0]['components'];
        $returnStreetAddress = $componentArray['primary_number'].' '.$componentArray['street_name'].' '.$componentArray['street_suffix'].'.';
        if (array_key_exists('secondary_designator', $componentArray)) {
          $returnStreetAddress2 = $componentArray['secondary_designator']. ' ' .$componentArray['secondary_number'];
        }
        else {
          $returnStreetAddress2 = FALSE;
        }
        $returnCity = $componentArray['city_name'];
        $returnState = $componentArray['state_abbreviation'];
        $returnZip = $componentArray['zipcode'];

        if (sirsi_getacardServiceZips($returnZip)) {
          $form_state->set('outOfArea', 0);
        } else {
          $form_state->set('outOfArea', 1);
          $outOfAreaBlockedCheck = $this->configFactory->get('sirsi_getacard.adminsettings')->get('disable_out_of_area');
          if($outOfAreaBlockedCheck == 1) {
            $outOfAreaMessage = $this->configFactory->get('sirsi_getacard.adminsettings')->get('out_of_area');
            $this->messenger()->addMessage($outOfAreaMessage);
            $form_state
              ->setValues($form_state->get('page1_values'))
              ->set('page_num', 1)
              ->setRebuild(TRUE);
            return;
          }
        }
        if ($returnZip == $form_state->getValue('zip')) {
          /* set correct vals from api */
          $form_state->setValue('street_add', $returnStreetAddress);
          if ($returnStreetAddress2 != FALSE) {
            $form_state->setValue('street_add_2', $returnStreetAddress2);
          }
          $form_state->setValue('city', $returnCity);
          $form_state->setValue('state', $returnState);
          $form_state->setValue('zip', $returnZip);
        }
      }
      /* unhoused residents */
    } else if ($street_add == 'GENERAL+DELIVERY+' && $zip == '20090') {
      return;
    }
    else {
      /* something went wrong... */
      $addressError = 'There was a problem with the address you used. Please check the address.';
      $this->messenger()->addWarning($addressError);
      $form_state->setRebuild(TRUE);
    }

  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * Submission handler for page 2.
   */
  public function createPage2Submit(array &$form, FormStateInterface $form_state) {
    $form2_vals = [
      'no_address' => $form_state->getValue('no_address'),
      'educator' => $form_state->getValue('educator'),
      'street_add' => $form_state->getValue('street_add'),
      'street_add_2' => $form_state->getValue('street_add_2'),
      'city' => $form_state->getValue('city'),
      'country' => $form_state->getValue('country'),
      'state' => $form_state->getValue('state'),
      'zip' => $form_state->getValue('zip'),
      'id_one' => $form_state->getValue('id_one'),
      'id_one_back' => $form_state->getValue('id_one_back'),
      'school_name' => $form_state->getValue('school_name'),
      'edu_street_add' => $form_state->getValue('edu_street_add'),
      'edu_street_add_2' => $form_state->getValue('edu_street_add_2'),
      'edu_city' => $form_state->getValue('edu_city'),
      'edu_state' => $form_state->getValue('edu_state'),
      'home_library' => $form_state->getValue('home_library'),
      'pin' => $form_state->getValue('pin'),
    ];
    $form_state
      ->set('page2_values', $form2_vals)
      ->set('page_num', 3)
      ->setRebuild(TRUE);
  }

  ########################## END Form 2 ##########################

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   * Builds the third step form (page 3).
   */
  public function createPageThree(array &$form, FormStateInterface $form_state) {
    $testMode = $this->configFactory->get('sirsi_getacard.adminsettings')->get('test_mode');
    if($testMode != 0) {
      $this->messenger()->addWarning('Signup Forms are in Test Mode');
      $required = FALSE;
    } else {
      $required = TRUE;
    }
    $allFormVals = array_merge($form_state->get('page1_values'), $form_state->get('page2_values'));
    $firstName = $allFormVals['first_name'];
    $middleIni = $allFormVals['mi'];
    $lastName = $allFormVals['last_name'];
    if($middleIni != NULL && $middleIni != '') {
      $fullName = $firstName.' '.$middleIni.' '.$lastName;
    }
    else {
      $fullName = $firstName.' '.$lastName;
    }
    if($form_state->get('underageFlag') == 1) {
      $pFirstName = $allFormVals['p_first_name'];
      $pMiddleIni = $allFormVals['p_mi'];
      $pLastName = $allFormVals['p_last_name'];
      if($pMiddleIni != NULL && $pMiddleIni != '') {
        $pFullName = $pFirstName.' '.$pMiddleIni.' '.$pLastName;
      }
      else {
        $pFullName = $pFirstName.' '.$pLastName;
      }
    }
    $form['sub_title'] = array(
      '#markup' => $this->t('Library card application and information about card benefits.'),
    );
    /* markup for tabs */
    $tabMarkup = '<div id="formTabs" class="tab tabsPage3">';
    $tabMarkup .= '<div id="formTab1" class="formbutton cell medium-auto completed">1. Personal Details</div>';
    $tabMarkup .= '<div id="formTab2" class="formbutton cell medium-auto completed">2. Address Verification</div>';
    $tabMarkup .= '<div id="formTab3" class="formbutton cell medium-auto active">3. Confirmation</div>';
    $tabMarkup .= '</div>';
    $form['tabs'] = array(
      '#markup' => $tabMarkup,
    );

    $form['sirsi-hidden'] = array(
      '#type' => 'hidden',
      '#value' => sirsi_getacardMakeUnhousedValues(),
    );

    $markup = '<div id="cardConfirm" class="getacardConfirm">';
    $markup .= '<div class="cardData">Summary:</div>';
    $markup .= '<div class="cardData">Full Name: '.$fullName.'</div>';
    $markup .= '<div class="cardData">DOB: '.$allFormVals['dob'].'</div>';
    $markup .= '<div class="cardData">Email: '.$allFormVals['email'].'</div>';
    $markup .= '<div class="cardData">Phone: '.$allFormVals['phone_number'].'</div>';
    if($form_state->get('underageFlag') == 1) {
      $markup .= '<div class="cardData">Parents Full Name: '.$pFullName.'</div>';
      $markup .= '<div class="cardData">Parents Email: '.$allFormVals['p_email'].'</div>';
      $markup .= '<div class="cardData">Parents Phone: '.$allFormVals['p_phone_number'].'</div>';
    }
    $markup .= '<div class="cardData">Address: '.$allFormVals['street_add'].'</div>';
    if($allFormVals['street_add_2'] != NULL || $allFormVals['street_add_2'] != '') {
      $markup .= '<div class="cardData">Address 2: '.$allFormVals['street_add_2'].'</div>';
    }
    $markup .= '<div class="cardData">City: '.$allFormVals['city'].'</div>';
    $markup .= '<div class="cardData">State: '.$allFormVals['state'].'</div>';
    $markup .= '<div class="cardData">Country: '.$allFormVals['country'].'</div>';
    $markup .= '<div class="cardData">Zip: '.$allFormVals['zip'].'</div>';
    $markup .= '</div>';

    $form['complete_info'] = array(
      '#markup' => $markup,
    );

    $disableCheckout = $this->configFactory->get('sirsi_getacard.adminsettings')->get('disable_checkout');
    if($disableCheckout != 1) {
      if ($form_state->has('outOfArea') && $form_state->get('outOfArea') == 1) {

        $form['cc_same_address'] = [
          '#type' => 'checkbox',
          '#title' => 'Use this as my billing address:',
          '#default_value' => $form_state->getValue('cc_same_address'),
        ];

        $form['cc_info'] = array(
          '#type' => 'fieldset',
          '#title' => $this->t('Credit Card Information'),
          '#open' => TRUE,
        );

        $form['cc_info']['cc_name'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Name on Credit Card'),
          '#default_value' => $form_state->getValue('cc_name'),
        ];

        $form['cc_info']['credit_card_number'] = array(
          '#type' => 'textfield',
          '#title' => $this->t('Credit Card Number'),
          '#maxlength' => 16,
        );

        $form['cc_info']['expiration_date'] = array(
          '#type' => 'textfield',
          '#title' => $this->t('Expiration Date'),
        );

        $form['cc_info']['credit_card_cvv'] = array(
          '#type' => 'textfield',
          '#title' => $this->t('CVV Code'),
          '#maxlength' => 4,
          '#description' => $this->t('3 or 4 digit security code.'),
        );

        $form['cc_info']['cc_street_add'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Street Address'),
          '#default_value' => $form_state->getValue('cc_street_add'),
          '#size' => 60,
          '#maxlength' => 128,
          '#states' => [
            'invisible' => [
              ':input[name="cc_same_address"]' => ['checked' => TRUE],
            ],
          ],
        ];

        $form['cc_info']['cc_street_add_2'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Street Address 2'),
          '#default_value' => $form_state->getValue('cc_street_add_2'),
          '#size' => 60,
          '#maxlength' => 128,
          '#states' => [
            'invisible' => [
              ':input[name="cc_same_address"]' => ['checked' => TRUE],
            ],
          ],
        ];

        $form['cc_info']['cc_city'] = [
          '#type' => 'textfield',
          '#title' => $this->t('City'),
          '#default_value' => $form_state->getValue('cc_city'),
          '#size' => 60,
          '#maxlength' => 128,
          '#states' => [
            'invisible' => [
              ':input[name="cc_same_address"]' => ['checked' => TRUE],
            ],
          ],
        ];

        $form['cc_info']['cc_state'] = [
          '#type' => 'select',
          '#title' => $this->t('State'),
          '#options' => sirsi_getacard_StatesDropdown(),
          '#empty_option' => $this->t('-select-'),
          '#default_value' => $form_state->getValue('cc_state'),
          '#states' => [
            'invisible' => [
              ':input[name="cc_same_address"]' => ['checked' => TRUE],
            ],
          ],
        ];

        $form['cc_info']['cc_country'] = [
          '#type' => 'select',
          '#title' => $this->t('Country'),
          '#options' => [
            'US' => $this->t('United States'),
          ],
          '#empty_option' => $this->t('-select-'),
          '#default_value' => $form_state->getValue('cc_country'),
          '#states' => [
            'invisible' => [
              ':input[name="cc_same_address"]' => ['checked' => TRUE],
            ],
          ],
        ];

        $form['cc_info']['cc_zip'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Zip Code'),
          '#default_value' => $form_state->getValue('cc_zip'),
          '#size' => 60,
          '#maxlength' => 128,
          '#states' => [
            'invisible' => [
              ':input[name="cc_same_address"]' => ['checked' => TRUE],
            ],
          ],
        ];
      }
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['back2'] = [
      '#type' => 'submit',
      '#value' => $this->t('Previous'),
      '#prefix' => '<div id="sirsi-clr-btn">',
      '#suffix' => '</div>',
      '#submit' => ['::createPageThreeBack'],
      '#limit_validation_errors' => [],
    ];

    $form['actions']['next3'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Finish'),
      '#prefix' => '<div id="sirsi-clr-btn">',
      '#suffix' => '</div>',
      '#validate' => ['::createNext3Validate'],
    ];

    $form['actions']['reset'] = [
      '#type' => 'button',
      '#value' => $this->t('Clear'),
      '#prefix' => '<div id="sirsi-reset-btn">',
      '#suffix' => '</div>',
      '#attributes' => ['onclick' => 'return false;'],
    ];
    return $form;
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * Custom submission handler for 'Back' button (page 3).
   */
  public function createPageThreeBack(array &$form, FormStateInterface $form_state) {
    $form_state
      ->setValues($form_state->get('page2_values'))
      ->set('page_num', 2)
      ->setRebuild(TRUE);
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * Validation handler for page 3.
   *
   */
  public function createNext3Validate(array &$form, FormStateInterface $form_state) {
    #
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $testMode = $this->configFactory->get('sirsi_getacard.adminsettings')->get('test_mode');
    ($testMode != 0) ? $required = FALSE : $required = TRUE;
    $allFormVals = array_merge($form_state->get('page1_values'), $form_state->get('page2_values'));
    $symphonyApiController = Drupal::service('sirsi_getacard_symphony.connector');
    ($required) ? $patronBarcode = $symphonyApiController->createPatron($allFormVals) : $patronBarcode = FALSE;
    if ($patronBarcode != FALSE) {
      $mailMessage = 'Hi ' . $allFormVals['firstName'];
      $mailMessage .= ' Your new library card number is ' . $patronBarcode;
      $mailMessage .= $this->configFactory->get('sirsi_getacard.adminsettings')->get('email_message_one');
      $to = $allFormVals['email'];
      $from = $this->config('system.site')->get('mail');
      $params = ['message' => $mailMessage];
      $language_code = $this->languageManager->getDefaultLanguage()->getId();

      $result = $this->mailManager->mail('sirsi_getacard', 'recover_account', $to, $language_code, $params, $from, TRUE);
      if ($result['result'] == TRUE) {
        Drupal::logger('sirsi_getacard')
          ->notice('A patron with the email address of ' . $to . ' has been created and assigned barcode ' . $patronBarcode);
      } else {
        Drupal::logger('sirsi_getacard')
          ->error('There was a problem sending an emailed message to ' . $to);
      }
      $response = new RedirectResponse('/getacard/complete/' . $patronBarcode);
      $response->send();
    } else {
      $this->messenger()->addWarning('There was a problem creating this account - Please check with the desk.');
    }

  }

}


