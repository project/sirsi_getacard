<?php
/**
 * @file
 * Contains \Drupal\sirsi_getacard\Form\sirsi_getacardSettings.
 */
namespace Drupal\sirsi_getacard\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class sirsi_getacardSettings extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SIRSI_GETACARD_SETTINGS = 'sirsi_getacard.adminsettings';

  /**
   * @return string
   *
   */
  public function getFormId() {
    return 'sirsi_getacard_settings_form';
  }

  /**
   * @return string[]
   *
   */
  protected function getEditableConfigNames() {
    return [
      static::SIRSI_GETACARD_SETTINGS,
    ];
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   *
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
		$config = $this->config(static::SIRSI_GETACARD_SETTINGS);

    $form['signups'] = [
      '#type' => 'details',
      '#title' => t('Get A Card Form Settings'),
      '#open' => FALSE,
    ];

    $form['signups']['test_mode'] = [
      '#type' => 'checkbox',
      '#title' => 'Put the signup forms in test mode',
      '#default_value' => $config->get('test_mode'),
    ];

    $form['signups']['disable_checkout'] = [
      '#type' => 'checkbox',
      '#title' => 'Disable the Credit Card checkout form fields.',
      '#default_value' => $config->get('disable_checkout'),
    ];

    $form['signups']['disable_out_of_area'] = [
      '#type' => 'checkbox',
      '#title' => 'Only allow verified zipcodes in the "Symphony / Zipcodes Served" settings field to get a card.',
      '#default_value' => $config->get('disable_out_of_area'),
    ];

    $form['signups']['out_of_area'] = [
      '#type' => 'textfield',
      '#title' => 'Out of Area Message',
      '#default_value' => $config->get('out_of_area'),
      '#required' => FALSE,
      '#description' => 'The message users not in the zipcode range get when they try and sign up.',
    ];

    $form['signups']['min_age'] = [
      '#type' => 'textfield',
      '#title' => 'Minimum Age for Signup',
      '#default_value' => $config->get('min_age'),
      '#required' => FALSE,
      '#description' => 'The minimum age an individual needs to be to signup without parental consent.',
    ];

    $form['smarty'] = [
      '#type' => 'details',
      '#title' => t('Smarty API Settings'),
      '#open' => FALSE,
    ];

    $form['smarty']['base_url'] = [
      '#type' => 'textfield',
      '#title' => 'Base URL',
      '#default_value' => $config->get('base_url'),
      '#required' => FALSE,
      '#description' => 'API URL with no ending slash ex: "https://somesite.com/rest/service".',
    ];

    $form['smarty']['auth_id'] = [
      '#type' => 'textfield',
      '#title' => 'Auth ID',
      '#default_value' => $config->get('auth_id'),
      '#required' => FALSE,
      '#description' => 'The API account Authorization Id.',
    ];

    $form['smarty']['auth_token'] = [
      '#type' => 'textfield',
      '#title' => 'Auth Token',
      '#default_value' => $config->get('auth_token'),
      '#required' => FALSE,
      '#description' => 'The API account authorization token.',
    ];

    $form['symphony'] = [
      '#type' => 'details',
      '#title' => t('Symphony API Settings'),
      '#open' => FALSE,
    ];

    $form['symphony']['api_url'] = [
      '#type' => 'textfield',
      '#title' => 'Base URL',
      '#default_value' => $config->get('api_url'),
      '#required' => FALSE,
      '#description' => 'API URL with no ending slash ex: "https://somesite.com/rest/service".',
    ];

    $form['symphony']['client_id'] = [
      '#type' => 'textfield',
      '#title' => 'x-sirs-clientID',
      '#default_value' => $config->get('client_id'),
      '#required' => FALSE,
      '#description' => '',
    ];

    $form['symphony']['webapp_id'] = [
      '#type' => 'textfield',
      '#title' => 'SD-Originating-App-Id',
      '#default_value' => $config->get('webapp_id'),
      '#required' => FALSE,
      '#description' => '',
    ];

    $form['symphony']['apiuserid'] = [
      '#type' => 'textfield',
      '#title' => 'API User Id',
      '#default_value' => $config->get('apiuserid'),
      '#required' => FALSE,
      '#description' => '',
    ];

    $form['symphony']['apipasswd'] = [
      '#type' => 'textfield',
      '#title' => 'API Password',
      '#default_value' => $config->get('apipasswd'),
      '#required' => FALSE,
      '#description' => '',
    ];

    $form['symphony']['zipcodes'] = array(
      '#type' => 'textarea',
      '#title' => 'Zipcodes Served',
      '#description' => 'A comma seperated list of zipcodes that can get a library card.',
      '#default_value' => $config->get('zipcodes'),
    );

    $form['symphony']['dropdown_locations'] = array(
      '#type' => 'textarea',
      '#title' => 'Library Branch Location Dropdown Values',
      '#description' => 'Use the format SOUTHLIBRARY|South Library followed by a new line for the dropdown.',
      '#default_value' => $config->get('dropdown_locations'),
    );

    $form['symphony']['language_field_key'] = [
      '#type' => 'textfield',
      '#title' => 'Patron language field name',
      '#default_value' => $config->get('language_field_key'),
      '#required' => FALSE,
      '#description' => 'The Sirsi Enterprise API key corresponding to the field where language preference is stored.',
    ];

    $form['symphony']['language_field_path'] = [
      '#type' => 'textfield',
      '#title' => 'Patron language field path',
      '#default_value' => $config->get('language_field_path'),
      '#required' => FALSE,
      '#description' => 'The Sirsi Enterprise API path string for the field where language preference is stored. Use the format /something/another/thing',
    ];

    $form['symphony']['auth_header_str'] = [
      '#type' => 'textfield',
      '#title' => 'Auth Header String',
      '#default_value' => $config->get('auth_header_str'),
      '#required' => FALSE,
      '#description' => 'The Sirsi Enterprise API auth string. ex: USER_PRIVILEGE_OVRCD/LAM1',
    ];

    $form['admin_home_add'] = array(
      '#type' => 'details',
      '#title' => 'Unhoused Home Address',
      '#open' => FALSE,
    );

    $form['admin_home_add']['admin_street_add'] = [
      '#type' => 'textfield',
      '#title' => 'Street Address',
      '#default_value' => $config->get('admin_street_add'),
      '#size' => 60,
      '#maxlength' => 128,
    ];

    $form['admin_home_add']['admin_street_add_2'] = [
      '#type' => 'textfield',
      '#title' => 'Street Address 2',
      '#default_value' => $config->get('admin_street_add_2'),
      '#size' => 60,
      '#maxlength' => 128,
    ];

    $form['admin_home_add']['admin_city'] = [
      '#type' => 'textfield',
      '#title' => 'City',
      '#default_value' => $config->get('admin_city'),
      '#size' => 60,
      '#maxlength' => 128,
    ];

    $form['admin_home_add']['admin_state'] = [
      '#type' => 'textfield',
      '#title' => 'State',
      '#default_value' => $config->get('admin_state'),
    ];

    $form['admin_home_add']['admin_country'] = [
      '#type' => 'textfield',
      '#title' => 'Country',
      '#default_value' => $config->get('admin_country'),
    ];

    $form['admin_home_add']['admin_zip'] = [
      '#type' => 'textfield',
      '#title' => 'Zip Code',
      '#default_value' => $config->get('admin_zip'),
      '#size' => 60,
      '#maxlength' => 128,
    ];

    $form['admin_email_message'] = array(
      '#type' => 'details',
      '#title' => 'Welcome email and message text',
      '#open' => FALSE,
    );

    $form['admin_email_message']['email_message_one'] = array(
      '#type' => 'textarea',
      '#title' => 'Email message.',
      '#description' => 'Email message for new patron.',
      '#default_value' => $config->get('email_message_one'),
    );

    $form['admin_email_message']['thankyou_message_one'] = array(
      '#type' => 'textarea',
      '#title' => 'Thank you message.',
      '#description' => 'Thank you page message for new patron sign-ups.',
      '#default_value' => $config->get('thankyou_message_one'),
    );

    $form['doclinks'] = array(
      '#type' => 'details',
      '#title' => 'Document Paths',
      '#open' => FALSE,
    );

    $form['doclinks']['termsofuse'] = array(
      '#type' => 'textfield',
      '#title' => 'Terms of Use.',
      '#description' => 'Path to the terms of use document. ex: "/path/indrupal/mydoc"',
      '#default_value' => $config->get('termsofuse'),
    );

    $form['doclinks']['privacy'] = array(
      '#type' => 'textfield',
      '#title' => 'Privacy Policy.',
      '#description' => 'Path to the privacy policy document. ex: "/path/indrupal/mydoc"',
      '#default_value' => $config->get('privacy'),
    );

    $form['doclinks']['enable_reg'] = [
      '#type' => 'checkbox',
      '#title' => 'Enable Voter Registration',
      '#default_value' => $config->get('enable_reg'),
    ];

    $form['doclinks']['voterreg'] = array(
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => 'Voter Registration.',
      '#description' => 'Path to the voter registration document. ex: "/path/indrupal/mydoc"',
      '#default_value' => $config->get('voterreg'),
    );

    $form['doclinks']['voterreg_img'] = [
      '#type' => 'managed_file',
      '#title' => 'Voter Registration Icon',
      '#description' => 'You can upload .jpg, .gif, .png, .txt, .doc, .xls - leave blank to use the default text link.',
      '#upload_location' => 'public://voter_registration',
      '#upload_validators' => [
        'file_validate_is_image' => array(),
        'file_validate_extensions' => array('jpg jpeg gif png txt doc xls pdf ppt pps odt ods odp'),
      ],
      '#default_value' => $config->get('voterreg_img'),
    ];

    $form['language'] = [
      '#type' => 'details',
      '#title' => t('Form Language Settings'),
      '#open' => FALSE,
    ];

    $form['language']['dropdown_languages'] = array(
      '#type' => 'textarea',
      '#title' => 'Language options for the form dropdown form element',
      '#description' => 'Use the format Amharic|Amharic followed by a new line for the dropdown.',
      '#default_value' => $config->get('dropdown_languages'),
    );

    $form['complete'] = [
      '#type' => 'details',
      '#title' => t('Completion Page Accordion Values'),
      '#open' => FALSE,
    ];

    $form['complete']['header_one'] = array(
      '#type' => 'textfield',
      '#title' => 'Heading One',
      '#description' => 'The heading of the first accordion section.',
      '#default_value' => $config->get('header_one'),
    );

    $form['complete']['text_one'] = array(
      '#type' => 'textarea',
      '#title' => 'Text Area One',
      '#description' => 'The content of the first accordion section.',
      '#default_value' => $config->get('text_one'),
    );

    $form['complete']['header_two'] = array(
      '#type' => 'textfield',
      '#title' => 'Heading Two',
      '#description' => 'The heading of the second accordion section.',
      '#default_value' => $config->get('header_two'),
    );

    $form['complete']['text_two'] = array(
      '#type' => 'textarea',
      '#title' => 'Text Area Two',
      '#description' => 'The content of the second accordion section.',
      '#default_value' => $config->get('text_two'),
    );

    $form['complete']['header_three'] = array(
      '#type' => 'textfield',
      '#title' => 'Heading Three',
      '#description' => 'The heading of the third accordion section.',
      '#default_value' => $config->get('header_three'),
    );

    $form['complete']['text_three'] = array(
      '#type' => 'textarea',
      '#title' => 'Text Area Three',
      '#description' => 'The content of the third accordion section.',
      '#default_value' => $config->get('text_three'),
    );

    $form['complete']['header_four'] = array(
      '#type' => 'textfield',
      '#title' => 'Heading Four',
      '#description' => 'The heading of the third accordion section.',
      '#default_value' => $config->get('header_four'),
    );

    $form['complete']['text_four'] = array(
      '#type' => 'textarea',
      '#title' => 'Text Area Four',
      '#description' => 'The content of the third accordion section.',
      '#default_value' => $config->get('text_four'),
    );

    $form['complete']['header_five'] = array(
      '#type' => 'textfield',
      '#title' => 'Heading Five',
      '#description' => 'The heading of the third accordion section.',
      '#default_value' => $config->get('header_five'),
    );

    $form['complete']['text_five'] = array(
      '#type' => 'textarea',
      '#title' => 'Text Area Five',
      '#description' => 'The content of the third accordion section.',
      '#default_value' => $config->get('text_five'),
    );

    $form['complete']['header_six'] = array(
      '#type' => 'textfield',
      '#title' => 'Heading Six',
      '#description' => 'The heading of the third accordion section.',
      '#default_value' => $config->get('header_six'),
    );

    $form['complete']['text_six'] = array(
      '#type' => 'textarea',
      '#title' => 'Text Area Six',
      '#description' => 'The content of the third accordion section.',
      '#default_value' => $config->get('text_six'),
    );

    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Save',
      '#button_type' => 'primary',
    );
    return parent::buildForm($form, $form_state);
	}

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    #
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SIRSI_GETACARD_SETTINGS)
      ->set('test_mode', $form_state->getValue('test_mode'))
      ->set('disable_checkout', $form_state->getValue('disable_checkout'))
      ->set('disable_out_of_area', $form_state->getValue('disable_out_of_area'))
      ->set('out_of_area', $form_state->getValue('out_of_area'))
      ->set('min_age', $form_state->getValue('min_age'))
      ->set('base_url', $form_state->getValue('base_url'))
      ->set('auth_id', $form_state->getValue('auth_id'))
      ->set('auth_token', $form_state->getValue('auth_token'))
      ->set('api_url', $form_state->getValue('api_url'))
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('webapp_id', $form_state->getValue('webapp_id'))
      ->set('apiuserid', $form_state->getValue('apiuserid'))
      ->set('apipasswd', $form_state->getValue('apipasswd'))
      ->set('zipcodes', $form_state->getValue('zipcodes'))
      ->set('dropdown_locations', $form_state->getValue('dropdown_locations'))
      ->set('language_field_key', $form_state->getValue('language_field_key'))
      ->set('language_field_path', $form_state->getValue('language_field_path'))
      ->set('auth_header_str', $form_state->getValue('auth_header_str'))
      ->set('admin_street_add', $form_state->getValue('admin_street_add'))
      ->set('admin_street_add_2', $form_state->getValue('admin_street_add_2'))
      ->set('admin_city', $form_state->getValue('admin_city'))
      ->set('admin_country', $form_state->getValue('admin_country'))
      ->set('admin_state', $form_state->getValue('admin_state'))
      ->set('admin_zip', $form_state->getValue('admin_zip'))
      ->set('email_message_one', $form_state->getValue('email_message_one'))
      ->set('thankyou_message_one', $form_state->getValue('thankyou_message_one'))
      ->set('termsofuse', $form_state->getValue('termsofuse'))
      ->set('privacy', $form_state->getValue('privacy'))
      ->set('enable_reg', $form_state->getValue('enable_reg'))
      ->set('voterreg', $form_state->getValue('voterreg'))
      ->set('voterreg_img', $form_state->getValue('voterreg_img'))
      ->set('dropdown_languages', $form_state->getValue('dropdown_languages'))
      ->set('header_one', $form_state->getValue('header_one'))
      ->set('text_one', $form_state->getValue('text_one'))
      ->set('header_two', $form_state->getValue('header_two'))
      ->set('text_two', $form_state->getValue('text_two'))
      ->set('header_three', $form_state->getValue('header_three'))
      ->set('text_three', $form_state->getValue('text_three'))
      ->set('header_four', $form_state->getValue('header_four'))
      ->set('text_four', $form_state->getValue('text_four'))
      ->set('header_five', $form_state->getValue('header_five'))
      ->set('text_five', $form_state->getValue('text_five'))
      ->set('header_six', $form_state->getValue('header_six'))
      ->set('text_six', $form_state->getValue('text_six'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
